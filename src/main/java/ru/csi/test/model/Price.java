package ru.csi.test.model;

import lombok.*;

import java.util.Date;


/**
 * Класс цены продукта
 */
@Getter
@Setter
@EqualsAndHashCode
public class Price {

    /**
     * Идентификатор в БД
     */
    private long id;
    /**
     * Код товара
     */
    private String productCode;
    /**
     * Номер цены
     */
    private int number;
    /**
     * Номер отдела
     */
    private int depart;
    /**
     * Дата начала действия цены
     */

    // Можно переделать на LocalDataTime совместно с end, без вреда для остального кода. Просто в задании в примере указана дата с временем а в типе данных Date
    private Date begin;
    /**
     * Дата окончания действия цены
     */
    private Date end;
    /**
     * Цена товара в копейках
     */
    private long value;

    /**
     * Конструктор со всеми полями кроме id
     */
    public Price(@NonNull String productCode, @NonNull int number, @NonNull int depart, @NonNull Date begin, @NonNull Date end, @NonNull long value) {
        if (begin.compareTo(end)>=0) {
            throw new IllegalArgumentException("Date \"begin\" can't be after Date \"end\"");
        }
        this.productCode = productCode;
        this.number = number;
        this.depart = depart;
        this.begin = begin;
        this.end = end;
        this.value = value;
    }

    @Override
    public String toString() {
        return "productCode = " + productCode
                + ", number = " + number
                + ", depart = " + depart
                + ", begin = " + begin
                + ", end = " + end
                + ", value = " + value + "\n";
    }
}
