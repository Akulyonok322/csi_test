package ru.csi.test.services;

import ru.csi.test.model.Price;
import ru.csi.test.model.TypeOfDateCrossing;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс для работы с ценами на продукцию
 */
public class PriceService {

    /**
     * Метод объединения имеющихся цен с вновь импортиованными
     *
     * @param oldPrices Список имеющихся цен в из БД
     * @param newPrices Список вновь поступивших цен
     * @return Объединённый новый список цен для записи в БД
     */
    public List<Price> updatePriceList(List<Price> oldPrices, List<Price> newPrices) {

        // Копирую объект, что бы не испортить входящий ссылочный объект.
        List<Price> copyOldPrices = new ArrayList<>(oldPrices);

        newPrices.forEach(newPrice -> {
            // удаляем из старого списка потенциально конкурирующие цены (цены с таким же productCode number depart)
            List<Price> pricesWithSameProductCodeNumberDepart = findPricesWithSameProductCodeNumberDepart(newPrice, copyOldPrices);
            copyOldPrices.removeAll(pricesWithSameProductCodeNumberDepart);

            // для каждой удалённой цены вносим изменения (если нужны) и возвращаем изменённые в список copyOldPrices
            pricesWithSameProductCodeNumberDepart.forEach(oldPrice -> copyOldPrices.addAll(editOldPrice(oldPrice, newPrice)));

            // добавляем новую цену в copyOldPrices
            copyOldPrices.add(newPrice);

        });
        return copyOldPrices;
    }

    /**
     * По значению новой цены в списке старых цен ищем цены с таким же кодом продукта, номером и отделом
     *
     * @param newPrice  новая цена по которой ищем старые
     * @param oldPrices список старых цен
     * @return Старые цены с аналогичными значениями кода продукта, номера и отдела, что и в новой цене
     */
    private List<Price> findPricesWithSameProductCodeNumberDepart(Price newPrice, List<Price> oldPrices) {
        return oldPrices.stream()
                .filter(price -> newPrice.getProductCode().equals(price.getProductCode())
                        && newPrice.getNumber() == price.getNumber()
                        && newPrice.getDepart() == price.getDepart()
                )
                .collect(Collectors.toList());
    }

    /**
     * Тип пересечения периода действия старой цены периодом действия новой цены
     *
     * @param newPrice новая цена
     * @param oldPrice старая цена
     * @return Тип перекрытия старой цены новой
     */
    private TypeOfDateCrossing typeOfDateCrossing(Price newPrice, Price oldPrice) {


        // Проверка отсуствия перекрытия дат
        // Если начало новой цены после конца старой или конец новой до начала старой
        if (newPrice.getBegin().compareTo(oldPrice.getEnd()) >= 0 || newPrice.getEnd().compareTo(oldPrice.getBegin()) <= 0) {
            return TypeOfDateCrossing.NOT;
        }

        // Проверка полного перекрытия новой ценой старую цену
        if (newPrice.getBegin().compareTo(oldPrice.getBegin()) <= 0 && newPrice.getEnd().compareTo(oldPrice.getEnd()) >= 0) {
            return TypeOfDateCrossing.FULL;
        }

        // Проверка перекрытия новой конца старой
        if (newPrice.getBegin().compareTo(oldPrice.getBegin()) > 0 && newPrice.getEnd().compareTo(oldPrice.getEnd()) >= 0) {
            return TypeOfDateCrossing.END;
        }

        // Проверка перекрытия новой начала старой
        if (newPrice.getBegin().compareTo(oldPrice.getBegin()) <= 0 && newPrice.getEnd().compareTo(oldPrice.getEnd()) < 0) {
            return TypeOfDateCrossing.BEGIN;
        }

        // Проверка перекрытия новой середины старой
        if (newPrice.getBegin().compareTo(oldPrice.getBegin()) > 0 && newPrice.getEnd().compareTo(oldPrice.getEnd()) < 0) {
            return TypeOfDateCrossing.MIDDLE;
        }

        return TypeOfDateCrossing.ERROR;
    }

    /**
     * Изменяем старую цену (или оставляем неизменной) в зависимости от поступившей новой.
     * Тип List, потому что старая запись в одном из случаев может разделится на две.
     *
     * @param oldPrice Старая цена
     * @param newPrice Новая цена
     * @return В случае необходимости удаления старой цены возращается пустой список.
     */
    private List<Price> editOldPrice(Price oldPrice, Price newPrice) {
        // получаем тип пересечения дат
        TypeOfDateCrossing typeOfDateCrossing = typeOfDateCrossing(newPrice, oldPrice);

        List<Price> result = new ArrayList<>();

        switch (typeOfDateCrossing) {
            case NOT -> {
                result.add(oldPrice);
                return result;
            }
            case FULL -> {
                return result;
            }
            case END -> {
                if (oldPrice.getValue() == newPrice.getValue()) {
                    newPrice.setBegin(oldPrice.getBegin());
                    return result;
                }
                oldPrice.setEnd(newPrice.getBegin());
                result.add(oldPrice);
                return result;
            }
            case BEGIN -> {
                if (oldPrice.getValue() == newPrice.getValue()) {
                    newPrice.setEnd(oldPrice.getEnd());
                    return result;
                }
                oldPrice.setBegin(newPrice.getEnd());
                result.add(oldPrice);
                return result;
            }
            case MIDDLE -> {
                if (oldPrice.getValue() == newPrice.getValue()) {
                    newPrice.setBegin(oldPrice.getBegin());
                    newPrice.setEnd(oldPrice.getEnd());
                    return result;
                }
                Price oldPriceRight = new Price(oldPrice.getProductCode(),
                        oldPrice.getNumber(),
                        oldPrice.getDepart(),
                        newPrice.getEnd(),
                        oldPrice.getEnd(),
                        oldPrice.getValue());
                oldPrice.setEnd(newPrice.getBegin());
                result.add(oldPrice);
                result.add(oldPriceRight);
                return result;
            }
        }
        return result;
    }
}
