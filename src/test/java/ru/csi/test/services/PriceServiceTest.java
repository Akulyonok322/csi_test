package ru.csi.test.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.csi.test.model.Price;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

class PriceServiceTest {

    PriceService priceService = new PriceService();

    @ParameterizedTest
    @MethodSource("regularTestCases")
    void updatePriceListTest(List<Price> oldPrices, List<Price> newPrices, List<Price> expectedResultPrices) {

        // Сортирую, что бы удобнее было смотреть отличия.
        // Такой метод не наглядный assertTrue(first.size() == second.size() && first.containsAll(second) && second.containsAll(first))
        List<Price> result = priceService.updatePriceList(oldPrices, newPrices);
        result.sort((Comparator.comparing(Price::getProductCode).thenComparing(Price::getNumber).thenComparing(Price::getDepart).thenComparing(Price::getBegin)));
        expectedResultPrices.sort((Comparator.comparing(Price::getProductCode).thenComparing(Price::getNumber).thenComparing(Price::getDepart).thenComparing(Price::getBegin)));
        Assertions.assertEquals(expectedResultPrices, result);
    }


    public static Stream<Arguments> regularTestCases() {

        // Тест из примера
        List<Price> old1 = new ArrayList<>();
        List<Price> new1 = new ArrayList<>();
        List<Price> expected1 = new ArrayList<>();

        old1.add(new Price("122856", 1, 1, Date.from(Instant.parse("2013-01-01T00:00:00.00Z")), Date.from(Instant.parse("2013-01-31T23:59:59.00Z")), 11000));
        old1.add(new Price("122856", 2, 1, Date.from(Instant.parse("2013-01-10T00:00:00.00Z")), Date.from(Instant.parse("2013-01-20T23:59:59.00Z")), 99000));
        old1.add(new Price("6654", 1, 2, Date.from(Instant.parse("2013-01-01T00:00:00.00Z")), Date.from(Instant.parse("2013-01-31T00:00:00.00Z")), 5000));

        new1.add(new Price("122856", 1, 1, Date.from(Instant.parse("2013-01-20T00:00:00.00Z")), Date.from(Instant.parse("2013-02-20T23:59:59.00Z")), 11000));
        new1.add(new Price("122856", 2, 1, Date.from(Instant.parse("2013-01-15T00:00:00.00Z")), Date.from(Instant.parse("2013-01-25T23:59:59.00Z")), 92000));
        new1.add(new Price("6654", 1, 2, Date.from(Instant.parse("2013-01-12T00:00:00.00Z")), Date.from(Instant.parse("2013-01-13T00:00:00.00Z")), 4000));

        expected1.add(new Price("122856", 1, 1, Date.from(Instant.parse("2013-01-01T00:00:00.00Z")), Date.from(Instant.parse("2013-02-20T23:59:59.00Z")), 11000));
        expected1.add(new Price("122856", 2, 1, Date.from(Instant.parse("2013-01-10T00:00:00.00Z")), Date.from(Instant.parse("2013-01-15T00:00:00.00Z")), 99000));
        expected1.add(new Price("122856", 2, 1, Date.from(Instant.parse("2013-01-15T00:00:00.00Z")), Date.from(Instant.parse("2013-01-25T23:59:59.00Z")), 92000));
        expected1.add(new Price("6654", 1, 2, Date.from(Instant.parse("2013-01-01T00:00:00.00Z")), Date.from(Instant.parse("2013-01-12T00:00:00.00Z")), 5000));
        expected1.add(new Price("6654", 1, 2, Date.from(Instant.parse("2013-01-12T00:00:00.00Z")), Date.from(Instant.parse("2013-01-13T00:00:00.00Z")), 4000));
        expected1.add(new Price("6654", 1, 2, Date.from(Instant.parse("2013-01-13T00:00:00.00Z")), Date.from(Instant.parse("2013-01-31T00:00:00.00Z")), 5000));


        // цены не перескаются
        List<Price> old2 = new ArrayList<>();
        List<Price> new2 = new ArrayList<>();
        List<Price> expected2 = new ArrayList<>();

        old2.add(new Price("1", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));

        new2.add(new Price("2", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        new2.add(new Price("1", 2, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        new2.add(new Price("1", 1, 2, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        new2.add(new Price("1", 1, 1, Date.from(Instant.parse("2021-01-01T00:00:00.00Z")), Date.from(Instant.parse("2021-02-28T00:00:00.00Z")), 111));

        expected2.add(new Price("1", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        expected2.add(new Price("2", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        expected2.add(new Price("1", 2, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        expected2.add(new Price("1", 1, 2, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        expected2.add(new Price("1", 1, 1, Date.from(Instant.parse("2021-01-01T00:00:00.00Z")), Date.from(Instant.parse("2021-02-28T00:00:00.00Z")), 111));

        // Полное перекрытие старой цены
        List<Price> old3 = new ArrayList<>();
        List<Price> new3 = new ArrayList<>();
        List<Price> expected3 = new ArrayList<>();

        old3.add(new Price("1", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));

        new3.add(new Price("1", 1, 1, Date.from(Instant.parse("2021-01-01T00:00:00.00Z")), Date.from(Instant.parse("2023-02-28T00:00:00.00Z")), 111));

        expected3.add(new Price("1", 1, 1, Date.from(Instant.parse("2021-01-01T00:00:00.00Z")), Date.from(Instant.parse("2023-02-28T00:00:00.00Z")), 111));

        // прочие проверки
        List<Price> old4 = new ArrayList<>();
        List<Price> new4 = new ArrayList<>();
        List<Price> expected4 = new ArrayList<>();

        old4.add(new Price("1", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        old4.add(new Price("2", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        old4.add(new Price("3", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));

        new4.add(new Price("1", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-01-10T00:00:00.00Z")), 111));
        new4.add(new Price("2", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-01-10T00:00:00.00Z")), 222));
        new4.add(new Price("3", 1, 1, Date.from(Instant.parse("2022-01-02T00:00:00.00Z")), Date.from(Instant.parse("2022-01-03T00:00:00.00Z")), 111));

        expected4.add(new Price("1", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        expected4.add(new Price("2", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-01-10T00:00:00.00Z")), 222));
        expected4.add(new Price("2", 1, 1, Date.from(Instant.parse("2022-01-10T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));
        expected4.add(new Price("3", 1, 1, Date.from(Instant.parse("2022-01-01T00:00:00.00Z")), Date.from(Instant.parse("2022-02-28T00:00:00.00Z")), 111));


        return Stream.of(
                Arguments.of(old1, new1, expected1),
                Arguments.of(old2, new2, expected2),
                Arguments.of(old3, new3, expected3),
                Arguments.of(old4, new4, expected4)

        );
    }

}
